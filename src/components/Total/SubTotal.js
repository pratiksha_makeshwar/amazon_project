import React from "react";
import { useStateValue } from "../stateProvider/StateProvider";
import { getBasketTotal } from "../reducer/reducer";
import "./subtotal.css";

function SubTotal() {
  const [{ basket,count }] = useStateValue();

  return (
    <div className="subtotal">
      <p>
        (Subtotal {basket.length * count} items) :{" "}
        <strong>Total:{getBasketTotal(basket)*count}</strong>
      </p>
      <p>
        <small className="subtotal__gift"></small>
        <input type="checkbox" />
        This order contains
      </p>
      
      </div>
  );
}
export default SubTotal;
