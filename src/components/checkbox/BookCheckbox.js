import React, { Component } from 'react';
import { products } from "../../Product";

const box = products.filter((o)=> o.category === "book")
  .map((a) => a.langauage)
  .map((b) =>  {
    if (b) {
      return {
        name: b,
        key: b,
        label: b,
      };
    }
  });

class BookCheckbox extends Component {
  render() {
    return (
      <div>
          {box.map((option) =>(
          <div>
            <input
              type="checkbox"
              name={option.name}
              onChange={this.props.changeModel}
            />
             <label htmlFor={option.key}>{option.label[0].toUpperCase() + option.label.substring(1)}</label>
          </div>
        ))}
      </div>
    )
  }
}

export default BookCheckbox