import React, { Component } from "react";
import { products } from "../../Product";


const box = products.filter((o)=> o.category === "mobile")
  .map((a) => a.brand)
  .map((b) =>  {
    if (b) {
      return {
        name: b,
        key: b,
        label: b,
      };
    }
  });
class Checkbox extends Component {
  render() {
    return (
      <div>
        {box.map((option) =>(
          <div>
            <input
              type="checkbox"
              name={option.name}
              id={option.key}
              onChange={this.props.changeModel}
            />
             <label>{option.label[0].toUpperCase() + option.label.substring(1)}</label>
          </div>
        ))}
      </div>
    );
  }
}
export default Checkbox;
