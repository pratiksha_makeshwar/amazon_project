import React, { useCallback } from "react";
import StarIcon from "@material-ui/icons/Star";
import { useStateValue } from "../stateProvider/StateProvider";
import "../checkProduct/CheckOutProduct.css"

function CheckOutOrder({ id, image, title, price, rating }) {
  const [{}, dispatch] = useStateValue();

  const removeFromOrder = useCallback(() => {
    dispatch({
      type: "REMOVE_FROM_ORDER",
      id,
    });
  },[id])

  return (
    <div className="checkoutProduct">
      <img src={image} alt="" />
      <div className="checkoutProduct__info">
        <p className="checkoutProduct__title">{title}</p>
        <p className="checkoutProduct__price">
          <small>Price:</small>
          <strong>{price}</strong>
        </p>
        <div className="checkoutProduct__rating">
          {Array(rating)
            .fill()
            .map((index) => (
              <StarIcon key={index} />
            ))}
        </div>
        <button onClick={removeFromOrder}>Remove from orders</button>
      </div>
    </div>
  );
}
export default CheckOutOrder;
