import React from "react";
import { useStateValue } from "../stateProvider/StateProvider";
import "../checkProduct/Checkout.css"
import CheckOutOrder from "./CheckOutOrder";

function Order() {
  const [{ order }] = useStateValue();
  return (
    <div className="checkout">
      <div className="checkout__left">
        {order?.length === 0 ? (
          <div>
            <h2>Your order basket is empty</h2>
            <p>
              You have no items in your basket. To buy one or add item to basket
              click the Buy Now button
            </p>
          </div>
        ) : (
          <div>
            <h2 className="checkout__title">Your Orders</h2>
            {order.map((item) => {
              console.log(item);
              return (
                <CheckOutOrder
                  id={item.id}
                  title={item.title}
                  image={item.image}
                  price={item.price}
                  rating={item.rating}
                />
              );
            })}
          </div>
        )}
      </div>
      {order?.length > 0 && (
        <div className="checkout__right">Your Total Orders: {order.length}</div>
      )}
    </div>
  );
}
export default Order;
