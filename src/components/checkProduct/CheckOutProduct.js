import React, {useCallback, useState } from "react";
import StarIcon from "@material-ui/icons/Star";
import "./CheckOutProduct.css";
import { useStateValue } from "../stateProvider/StateProvider";

function CheckOutProduct({ id, image, title, price, rating }) {
  const [{}, dispatch] = useStateValue();
  const [count1, setCount1] = useState(1);
  
  const removeFromBasket = useCallback (() => {
    dispatch({
      type: "REMOVE_FROM_BASKET",
      id,
    });
  },[id])
  
  const addToOrders = () => {
  dispatch({
      type: "ADD_TO_ORDERS",
      item: {
        id,
        title,
        image,
        price,
        rating,
      },
    });
  }


  const increase = () => {
    setCount1(count1 + 1);
    dispatch({
      type: "INCREASE",

      status: count1 + 1,
    });
  }
  

  const decrease = () => {
    setCount1(count1 - 1);

    dispatch({
      type: "DECREASE",
      status: count1 - 1,
    });
  };
  
  return (
    <div className="checkoutProduct">
      <img classname="checkoutProduct__image" src={image} alt="" />
      <div className="checkoutProduct__info">
        <p className="checkoutProduct__title">{title}</p>
        <p className="checkoutProduct__price">
          <small>Price:</small>
          <strong>{price}</strong>
        </p>
        <div className="checkoutProduct__rating">
          {Array(rating)
            .fill()
            .map((index) => (
              <StarIcon key={index} />
            ))}
        </div>

        <p>
          {" "}
          Quantity<button onClick={decrease}>-</button>
          {count1}
          <button onClick={increase}>+</button>{" "}
        </p>
        <button onClick={removeFromBasket}>Remove from basket</button>
        <p>
          {" "}
          <button onClick={addToOrders} className="button">
            Buy Now
          </button>
        </p>
      </div>
    </div>
  );
}
export default CheckOutProduct;
