import React, { Component } from "react";
import "../home/Home.css"
import Product from "../basket/Product"
import { products } from "../../Product"
import BookCheckbox from "../checkbox/BookCheckbox";

class Book extends Component {
  state = {
    mobile: products,
    flag: false,
  };

  changeModel = (e) => {
    console.log("target", e.target, this.state.mobile);
    let arr = this.state.mobile.map((o) => {
      if (o.langauage === e.target.name){
        o.checked = !o.checked;
        }
         return o;
    });
    console.log("mobile", arr);
    this.setState({
      mobile: arr,
      flag: !this.state.flag
    })};

  render() {
    return (
      <React.Fragment>
      <div>
          <h4>Langauage</h4>
           <BookCheckbox changeModel={this.changeModel}/>
           </div>
      <div className="home">
      <div className="home__container">
        <div className="home__row">
        {this.state.flag ? this.state.mobile
              .filter((a) => a.checked)
              .map((item) => {
                return (
                  <Product
                    id={item.id}
                    title={item.title}
                    image={item.image}
                    price={item.price}
                    rating={item.rating}
                  />
                );
              }) : this.state.mobile.filter((o) => o.category === "book")
              .map((item) => {
                return (
                  <Product
                    id={item.id}
                    title={item.title}
                    image={item.image}
                    price={item.price}
                    rating={item.rating}
                  />
                );
              })  }
        </div>
        </div>
      </div>
      </React.Fragment>
    );
  }
}

export default Book;
