export const initialState = {
  basket: [],
  user:[],
  order: [],
  login: false,
  count: [1],
};


export const getBasketTotal = (basket) =>
  basket?.reduce((amount, item) => item.price  + amount, 0);


function reducer(state, action) {
  switch (action.type) {
    case "ADD_USER":
      return {
        ...state,
        user: [...state.user, action.user],
      };
      case "LOGIN": 
      return {
        ...state,
      login: [action.loginstatus],
      }
     
  case "ADD_TO_BASKET":
      return {
        ...state,
        basket: [...state.basket, action.item],
      };
    case "ADD_TO_ORDERS":
      return {
        ...state,
        order: [...state.order, action.item],
      };
    case "REMOVE_FROM_BASKET":
      let newBasket = [...state.basket];
      const index = state.basket.findIndex(
        (basketItem) => basketItem.id === action.id
      );
      if (index >= 0) {
        newBasket.splice(index, 1);
      } else {
        console.warn(
          `can't remove product{id: {action.id}} as it is not in the basket`
        );
      }
      return {
        ...state,
        basket: newBasket,
      };
      case "REMOVE_FROM_ORDER":
        let newOrder = [...state.order];
        const index1 = state.order.findIndex(
          (orderItem) => orderItem.id === action.id
        );
        if(index1 >=0){
          newOrder.splice(index1,1);
        } else{
          console.warn(
            `can't remove product{id: {action.id}} as it is not in the basket` 
          )
        }
        return {
          ...state,
          order: newOrder,
        }
        case "INCREASE":
      return {
            ...state,
            count: [action.status]
          }
          case "DECREASE":
        return{
            ...state,
            count : [action.status]
          }
          case "CLAER":
            return{
              ...state,
              count: 1
            }
        
      default:
      return state;
  }
}
export default reducer;
