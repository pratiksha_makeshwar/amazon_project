import React, {useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useStateValue } from "../stateProvider/StateProvider";
import "../user/Login.css"

function Register() {
  const [{}, dispatch] = useStateValue();
  const Navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [address, setAddress]= useState("")

  useEffect(()=> {
    console.log("register useeffect");
  },[name,email,password,address])

  const register = (event) => {
    event.preventDefault();
    dispatch({
      type: "ADD_USER",
      user: {
        name: name,
        email: email,
        password: password,
        address:address,
      },
    });
    if (name !== "" || password !== "" || email !== "") Navigate("/");
    else return alert("Please fill all the fields");
  };

  return (
    <div className="login">
      <Link to="/">
        <img className="login__logo" src="Images/amazon_logo.jpeg" alt="" />
      </Link>
      <div className="login__container">
        <h1>Create Account</h1>
        <form onSubmit={register}>
          <h5>Name:</h5>
          <input
            value={name}
            onChange={(event) => setName(event.target.value)}
            type="text"
          />
          <h5>Email:</h5>
          <input
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            type="email"
          />
          <h5>Password:</h5>
          <input
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            type="password"
          />
           <h5>Address:</h5>
          <input
            value={address}
            onChange={(event) => setAddress(event.target.value)}
            type="text"
          />
          <button type="submit" className="login__signInBtn">
            {" "}
            Register
          </button>
          <p>
            By signing, you agree to Amazon's Conditions of Use and Privacy
            Notice.
          </p>
        </form>
      </div>
      <p className="login__p">Already Have account?</p>
      <Link to="/login">
        <p>Sign In</p>
      </Link>
    </div>
  );
}
export default Register;
