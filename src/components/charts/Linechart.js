import React from 'react';
import {Line} from 'react-chartjs-2';
import './chart.css'
import {products} from '../../Product'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    } from 'chart.js';

    ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    
 );

function Linechart() {
   
  const options = {
        plugins: {
         title: {
            display: true,
            text: 'Line Chart',
          },
          scales: {
            yAxes: [
                {
                ticks: 
                    {
                      min: 0,
                      max: 6,
                      stepSize: 1
                    }
              
         } ]
        }
         
        },
      
      };
    const data = {
        labels: ['Jan','Feb','Mar','Apr','May'],
        datasets: [
            {
                label: 'Sales for 2020',
                data:products.map((o) => o.rating),
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
            },
            {
                label: 'Sales for 2022',
                data:[3,6,2,3,4],
                borderColor: 'rgb(53, 162, 235)',
                backgroundColor: 'rgba(53, 162, 235, 0.5)',
            }
        ]
    }
  return (
      <div className='chart'>
    <Line data={data} options={options} />
    </div>
    
  )
}

export default Linechart;