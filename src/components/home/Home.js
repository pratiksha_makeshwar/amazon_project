import React from "react";
import "./Home.css";
import Product from "../basket/Product";
import { products } from "../../Product";

function Home(props) {
 
  return (
    <div className="home">
      <div className="home__container">
        <img className="home__image" src="Images/banner.jpeg" alt="" />
      </div>
    <div className="home__row">
        {props.search.length >1 
          ? props.filterProducts.map((item) => {
              return (
                <Product
                  id={item.id}
                  title={item.title}
                  image={item.image}
                  price={item.price}
                  rating={item.rating}
                />
              );
            })
          : products.map((item) => {
              return (
                <Product
                  id={item.id}
                  title={item.title}
                  image={item.image}
                  price={item.price}
                  rating={item.rating}
                 />
              );
            })}
      </div>
    </div>
  );
}
export default Home;
