import React from "react";
import StarIcon from "@material-ui/icons/Star";
import { useNavigate } from "react-router-dom";
import "./Product.css";
import { useStateValue } from "../stateProvider/StateProvider";


function Product({ id, title, image, price, rating}) {
  const [{login}, dispatch] = useStateValue();
  const Navigate = useNavigate();


  const addToBasket = () => {
    if(login) {
    dispatch({
      type: "ADD_TO_BASKET",
      item: {
        id,
        title,
        image,
        price,
        rating,
        },
    })}
    else 
      Navigate("/login")
}

  return (
    <React.Fragment>
      <div className="product">
        <div className="product__info">
          <p>{title}</p>
          <p className="product__price">
            <small>Price:</small>
            <strong>{price}</strong>
          </p>
          <div className="product__rating">
            {Array(rating)
              .fill()
              .map((index) => (
                <StarIcon key={index} className="product__rating" />
              ))}
          </div>
        </div>
        <img src={image} alt="" />
        <button className="button" onClick={addToBasket}>Add to cart</button>
      </div>
    </React.Fragment>
  );
}
export default Product;
