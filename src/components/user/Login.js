import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Login.css";
import { useStateValue } from "../stateProvider/StateProvider";

function Login() {
  const [{ user, login }, dispatch] = useStateValue();
  const Navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  
console.log(user);

useEffect(() => {
  console.log('useeffect')
},[email,password])
 const handleSubmit = (event) => {
    event.preventDefault();
    if (email === "" || password === "") {
      return alert("Please fill all the fields");
    }
    let arr = user.map((o) => {
      if (email === o.email && password === o.password) {
        Navigate("/");
        dispatch({
          type: "LOGIN",
          loginstatus: !login,
      });
       
        
      }
     return o;
    
    });
    
    let arr1 = user.map((item) => {
      if (email !== item.email || password !== item.password) {
        alert("Invalid data");
      }
      return item;
    });
  };
  console.log(login);
  
  return (
    <div className="login">
      <Link to="/">
        <img className="login__logo" src="Images/amazon_logo.jpeg" alt="" />
      </Link>
      <div className="login__container">
        <h1>Sign-In</h1>
        <form onSubmit={handleSubmit}>
          <h5>Email:</h5>
          <input
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            type="email"
          />
          <h5>Password:</h5>
          <input
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            type="password"
          />
          <button type="submit" className="login__signInBtn">
            Sign-In
          </button>
          <p>
            By signing, you agree to Amazon's Conditions of Use and
            PrivacyNotice.
          </p>
        </form>
      </div>
      <p className="login__p">New to Amazon?</p>
      <Link to="/register">
        <p>Create Your Amazon Account</p>
      </Link>
    </div>
  );
}
export default Login;
