import React, { useState } from "react";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import LocationOnIcon from '@material-ui/icons/LocationOn'
import "./Header.css";
import { useStateValue } from "../stateProvider/StateProvider";
import Popup from "../popupwindow/Popup"

function Header(props) {  
  const [{basket,login,user}] = useStateValue();
  const [isOpen, setIsOpen]= useState(false);
  const [address, setAddress]=useState(user.map((o) => o.address));
 
  const togglePopup = () => {
   setIsOpen(!isOpen)
}
return (
    <React.Fragment>
    <div className="header">
      <Link to="/">
        <img
          className="header__logo"
        src="https://rdwgroup.com/wp-content/uploads/2013/08/Amazon-800x450-1.jpg" alt="amazon logo"
        />
      </Link>
      <div>
      <div className="header__option" onClick={togglePopup}>
            <span className="header__optionLineOne">Hello</span>
          
            <span className="header__optionLineTwo" ><LocationOnIcon/>{login ? address : "Select Your Address" }</span>
            </div>
          {isOpen && <Popup 
              content= {<>
                <b>Choose Your location</b>
                <p>select a delivery location</p>
                <Link to="/login"><button >Sign in to see your location</button></Link>
                <h5>Edit Your Address</h5>
                <h5>Address</h5>
                  <input placeholder="address" type="text" value={address} 
                  onChange={(event) => setAddress(event.target.value)} />
                  </>}
              handleClose={togglePopup}
              />}
      </div>
      
     <div className="header__search">
     
        <input className="header__searchInput" type="text" onChange={(e) => {
          props.setSearch(e.target.value.toLowerCase()); }} />
          <SearchIcon className="header__searchIcon" />
      </div>
    <div className="header__nav">
        <Link to="/login" className="header__link">
          <div className="header__option">
            <span className="header__optionLineOne">Hello</span>
            <span className="header__optionLineTwo">{login ? "Signout" : "Signin"}</span>
          </div>
          </Link>
       <Link to="/order" className="header__link">
          <div className="header__option">
            <span className="header__optionLineOne">Returns</span>
            <span className="header__optionLineTwo">& Orders</span>
          </div>
          </Link>
        <Link to="/checkout" className="header__link">
          <div className="header__optionBasket">
            <ShoppingBasketIcon />
            <span className="header__optionLineTwo header__basketCount">
              {basket?.length}
            </span>
          </div>
          </Link>
      </div>
    </div>
   </React.Fragment>
  );
}
export default Header;