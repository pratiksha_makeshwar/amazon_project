import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";


function SubHeader() {
  return (
    <React.Fragment>
      <div className="sub__header">
        <div className="header__nav">
          <Link to="/" className="header__link">
            <div className="header__option">
              <span className="header__optionLineTwo">All</span>
            </div>
          </Link>
        <div className="header__option">
            <span className="header__optionLineTwo">Best Sellers</span>
          </div>
          <Link to="/mobile" className="header__link">
            <div className="header__option">
              <span className="header__optionLineTwo">Mobiles</span>
            </div>
          </Link>
        <div className="header__option">
            <span className="header__optionLineTwo">Today's Deal</span>
          </div>
         <div className="header__option">
            <span className="header__optionLineTwo">Customer Service</span>
          </div>
         <div className="header__option">
            <span className="header__optionLineTwo">Electronice</span>
          </div>
        <div className="header__option">
            <span className="header__optionLineTwo">Fashion</span>
          </div>
          <Link to="/books" className="header__link">
           <div className="header__option">
            <span className="header__optionLineTwo">Books</span>
          </div>
          </Link>
          <Link to="/sales" className="header__link">
           <div className="header__option">
            <span className="header__optionLineTwo">Sales</span>
          </div>
          </Link>
        </div>
      </div>
    </React.Fragment>
  );
}
export default SubHeader;
