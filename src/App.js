import React, { useState } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./components/home/Home";
import Header from "./components/Navbar/Header";
import Register from "./components/registeration/Register";
import Checkout from "./components/checkProduct/Checkout";
import Mobile from "./components/pages/Mobile";
import SubHeader from "./components/Navbar/SubHeader";
import Order from "./components/order/Order";
import { products } from "./Product";
import Book from "./components/pages/Book";
import Login from "./components/user/Login";
import Linechart from "./components/charts/Linechart";

function App() {
  const [search, setSearch] = useState("");
  
  const filterProducts = products.filter((item) => {
    if (item.category === search || item.brand === search) {
      return item;
    }
  });
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route
            path="/"
            element={[
              <Header setSearch={setSearch}/>,
              <SubHeader />,
              <Home filterProducts={filterProducts} search={search} />,
            ]}
          />
          <Route path="/login" element={<Login/>} />
          <Route path="/register" element={<Register />} />
          <Route path="/checkout" element={[<Header />, <Checkout />]} />
          <Route
            path="/mobile"
            element={[<Header />, <SubHeader />, <Mobile />]}/>
          <Route path="/order" element={[<Header />, <Order />]} />
          <Route
            path="/books"
            element={[<Header />, <SubHeader />, <Book/>]}/>
            <Route path="/sales" element={<Linechart/>} />
         </Routes>
      </Router>
    </div>
  );
}
export default App;
