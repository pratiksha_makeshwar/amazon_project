export const products = [
    {
        id: 1,
        title: "Samsung Galaxy M12",
        image: "Images/samsung.jpeg",
        price: 10000,
        rating: 4,
        category: "mobile",
        brand: "samsung",
        
    },
    {
        id: 2,
        title: "Redmi Note 10T 5G",
        image: "Images/redmi.jpeg",
        price: 14999,
        rating: 5,
        category: "mobile",
        brand: "redmi",
        

    },
  {
      id: 3,
        title: "Smart Watch",
        image: "Images/watch.jpeg",
        price: 5999,
        rating: 5,
        category: "watch",
        brand:"sony"
       

    },
   
    {
       id: 4,
        title: "boAt Stone Portable Wireless Speaker",
        image: "Images/boatspeaker.jpeg",
        price: 40000,
        rating: 5,
        category: "audio",
        brand:"boat"
       
    },
    {
         id: 5,
        title: "Oculus Quest All-in-one VR Gaming Headset – 64GB",
        image: "Images/game.jpeg",
        price: 23999,
        rating: 1,
        category: "game",
        brand:"tata"
       
    },
    {
        id: 6,
        title: "Xbox game",
        image: "https://images-na.ssl-images-amazon.com/images/G/01/amazonglobal/images/email/asins/DURM-2B5ECC8E3DA42415._V531815325_.jpg",
        price: 4000,
        rating: 5,
        category: "game",
        brand:"tata"
       
    },
    {
       id: 7,
        title: "The Lean Startup: How Constant Innovation Creates Radically Successful Businesses Paperback",
        image: "Images/lean.jpg",
        price: 999,
        rating: 4,
        category: "book",
        langauage:"marathi"
       
    },
    {
        id: 8,
        title: "Play station game pad",
        image: "Images/pad.jpeg",
        price: 2000,
        rating: 5,
        category: "game",
        brand:"onida"
       
    },
    
    {
        id: 10,
           title: "House of Sky And Breath",
           image: "Images/House_of_sky.jpeg",
           price: 543,
           rating: 5,
           category: "book",
           langauage:"english"
          
        },
        {
    id: 11,
    title: "The Lifestyle Investor",
    image: "Images/lifestylebook.jpeg",
    price: 258,
    rating: 3,
    category: "book",
    langauage:"marathi"
 },    
]

